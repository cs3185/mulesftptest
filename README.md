# README #

MuleSftpTest is a template example.  The mule flow itself is simply to transfer a file from one sftp location to another.  The actual purpose is to demonstrate using Apache Mina SSH as a local test tool for functionally testing the flow in a portable, non-access dependent way.

### What is this repository for? ###

* The repository has the mule flow for point to point SFTP file transfer, and a FunctionalMuleSuite test class which will exercise the flow in a repeatable manner at build time, independent of network access.
* 1.0.0-SNAPSHOT

### How do I get set up? ###

* Configuration:  Built with Mule runtime 3.7.3 and java 8 but no parts should be version specific
* Dependencies:  Uses Apache Mina SSH server and extends the FunctionalMuleSuite.  Required maven dependencies include:  	
org.mule.tests mule-tests-functional
org.mule.munit munit-runner
org.apache.sshd sshd-sftp
org.apache.sshd sshd-core
org.vngx vngx-jsch
All scoped to test.

* How to run tests – should run automatically for a maven build or deploy or may be run manually as a junit test.
* Instructions:  Tests use override.properties to replace sftp values such as host, port, and paths with local and test values.  The server for test will use “testuser” and “testpassword” as the default authorization, other values will be rejected.  A base directory needs defined in override.  This will be used, relative to the System temp directory to create unique paths for testing with a source and destination added.  For the simple example test, a file is created in the source path, a delay occurs to give the Mule flow time to process the file, then the test verifies that the file is moved from source to dest.  In practice, this test should be modified as appropriate to validate correct function of the real flow.  When tests are complete, the server is stopped so will no longer be accessible via SFTP, and the paths created for the test are deleted.

### Notes: 

* The Apache SSH server uses the local native file structure.  So, while it is up and running, the paths can be seen either in the local file system or through an SFTP interface.  However, Filezilla failed for me because it is running FTP, not SFTP so is rejected.  Putty failed for me because the Apache server does not accept SSH Shell sessions.  It logs in, but is kicked right back off.  File Explorer could see the directories though.

* Failure to delete the paths after test will leave test data accessible after the tests.

* With the server, exceptions occur on connection close.  This is normal but annoying.  In the log4j2-test.xml file the line 
"<AsyncLogger name="org.apache.sshd.server.session" level="ERROR"/>" causes most of these to not be reported.  At server stop however, there is an intermittant error, sometimes which is reported via printlines rather than logging so cannot be suppressed.  This can be ignored.