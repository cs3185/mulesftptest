package appache.mina.sshd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.SshServer;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.common.Session;
import org.apache.sshd.common.file.FileSystemView;
import org.apache.sshd.common.file.nativefs.NativeFileSystemFactory;
import org.apache.sshd.common.file.nativefs.NativeFileSystemView;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.PasswordAuthenticator;
import org.apache.sshd.server.command.ScpCommandFactory;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.sftp.subsystem.SftpSubsystem;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mule.api.MuleContext;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.vngx.jsch.ChannelSftp;
import org.vngx.jsch.ChannelType;
import org.vngx.jsch.JSch;
import org.vngx.jsch.config.SessionConfig;
import org.vngx.jsch.exception.JSchException;
import org.vngx.jsch.exception.SftpException;

/**
 * 
 * @author CS3185
 * 
 * Sample functional test for SFTP connection with portability and no need for network
 * access or security issues using the Apache Mina SSH library to create an SFTP server
 * in memory for the tests.  Directories will be create in the temp folder of the local
 * OS to act as the remote file structure and deleted when done.  A file will be placed 
 * in the defined source directory and a wait will occur to allow the Mule flow to process it.
 * For this simple test the only assertion is to verify the file makes it to the destination
 * folder.  In use, far more complex data can be initiated and testing that correct transformation
 * occur or that can be left to the unit tests.
 *
 */
@RunWith(JUnit4.class)
public class SftpServerTest extends FunctionalMunitSuite {

    private static final String USERNAME = "testuser";
    
    private static final String PASSWORD = "testpassword";

    private static SshServer sshd = SshServer.setUpDefaultServer();

	private static String sourcePath;
	private static String destPath;
	private static String basePath;
	private static String userPath;
	private static String tempPath;

	
	private static File sourcePathFile;
	private static File destPathFile;
	
	private static File hostkeys;
	
	private static Properties prop;
	private static Integer port;
	private static String host;
	
	/**
	 *  Define flow to be tested.
	 */
	@Override
	protected String getConfigResources() {
	  return "minatest.xml";
	}
	
	
	/**
	 * Allow Mule endpoints to execute. 
	 */
	@Override
	protected boolean haveToDisableInboundEndpoints() {
		return false;
	}
	
	/**
	 * Allow Mule connectors to execute.
	 */
	@Override
	protected boolean haveToMockMuleConnectors() {
	  return false;
	}
	
	/**
	 * Let test create a Mule context for test.
	 */
	@Override
	protected void muleContextStarted(MuleContext muleContext) {
	  System.out.println("Mule Context Started at" + new Date(muleContext.getStartDate()));
	}
	
	/**
	 * 
	 * Runs before Mule and any tests to create the Apache Mina server instance.
	 * 
	 * 
	 * @throws IOException
	 */
	@BeforeClass
	public static void setup() throws IOException {
		initProps();
		initPaths();
		
	    hostkeys = new File(tempPath + basePath + "/hostKey.ser");
	    hostkeys.createNewFile();
	    
	    setupSSHServer();

	}
	
	/**
	 * Initializes Paths used by the server according to properties.
	 * 
	 * Paths used will be basePath relative to the System tempPath, plus a 
	 * SourceDir and DestinationDir beyond the basePath.  Any directory "basePath" 
	 * and below will be deleted if it exists before the call.
	 * 
	 * @throws IOException
	 */
	private static void initPaths() throws IOException {
		// Get the system temp file location.
		tempPath = System.getProperty("java.io.tmpdir");
		
		// The base path is relative to the temporary path.
		userPath = tempPath + basePath;

		// Delete the base path and below if it exists to start with empty directories.
		File makeFile = new File(tempPath + basePath);
		if (makeFile.exists()) {
			FileUtils.deleteDirectory(makeFile);
		}
		
		// Split the path for sub-directories which need created.
		String[] paths = basePath.split("/");

		// Create the basePath tree
		String makePath = tempPath;
		for (String path : paths) {
			makePath = makePath + path;
			makeFile = new File(makePath);
			makeFile.mkdir();
			makePath += "/";
		}
		
		// Add SourceDir and DestDir to the tree.
		sourcePath = userPath + "/SourceDir";
		sourcePathFile = new File(sourcePath);
		sourcePathFile.mkdir();
		
		destPath = userPath + "/DestDir";
		destPathFile = new File(destPath);
		destPathFile.mkdir();
	}
	
	// Get needed property values from the override file for testing.
	private static void initProps() throws IOException {
		// Get the pathing and sftp parameters from the override properties (test values) file.
		prop = new Properties();
		InputStream propStrm = new FileInputStream("src/test/resources/override.properties");
		
		prop.load(propStrm);
		
		basePath =  prop.getProperty("sftp.basePath");
		port = Integer.valueOf(prop.getProperty("sftp.port"));
		host = prop.getProperty("sftp.host");
	}
	
	// Setup the SSH Server for SFTP usage.
	private static void setupSSHServer() throws IOException {
		// Set server to actually use the native file system for SFTP access
	    sshd.setFileSystemFactory(new NativeFileSystemFactory() {
	    	@Override
	    	public FileSystemView createFileSystemView(final Session session) {
	    		return new NativeFileSystemView(session.getUsername(), false) {
	    			@Override
	    			public String getVirtualUserDir() {
	    				return sourcePathFile.getAbsolutePath();
	    			}
	    		};
	    	};
	    });
	    
	    //Establish listening port from property
	    sshd.setPort(port);
	    sshd.setSubsystemFactories(Arrays.<NamedFactory<Command>>asList(new SftpSubsystem.Factory()));
	    sshd.setCommandFactory(new ScpCommandFactory());

	    // Set up to validate authorization.  As done, only USERNAME and PASSWORD will be authorized.
	    sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(hostkeys.getAbsolutePath()));
	    sshd.setPasswordAuthenticator(new PasswordAuthenticator() {
	 		@Override
			public boolean authenticate(final String username, final String password, final ServerSession session) {
               return StringUtils.equals(username, USERNAME) && StringUtils.equals(password, PASSWORD);
			}
	    });
	    
	    sshd.start();
	}

	
	/**
	 * Create a file and populate it at the SFTP SourceDir path.
	 * 
	 * @param filename  		String filename to create.
	 * @param contents			String to populate the file with.
	 * @throws JSchException
	 * @throws SftpException
	 * @throws IOException
	 */
    private void sendFile(final String filename, final String contents) throws JSchException, SftpException, IOException {
    	SessionConfig config = new SessionConfig();
        config.setProperty(SessionConfig.STRICT_HOST_KEY_CHECKING, "no");
        org.vngx.jsch.Session session;

		session = JSch.getInstance().createSession(USERNAME, host, port, config);
    
        session.connect(PASSWORD.getBytes(StandardCharsets.UTF_8));
        ChannelSftp sftpChannel = session.openChannel(ChannelType.SFTP);
        sftpChannel.connect();
        
        OutputStream out = sftpChannel.put(filename);
        out.write(contents.getBytes(StandardCharsets.UTF_8));
        IOUtils.closeQuietly(out);
        sftpChannel.disconnect();
        session.disconnect();
    }

    /**
     * A sample test using the SFTP server to excercize a simple flow.
     * 
     * @throws Exception
     */
	@Test
	public void testMe() throws Exception {

		// Create a test file in SourceDir
        sendFile("test.txt", "abc");
	
        // Wait 20 seconds to allow the Mule flow to process the file.
		Thread.sleep(20000);
		
		// Verify the file has been processed to DestDir
		File destFile = new File(destPath + "/test.txt");
		Assert.assertTrue(destFile.exists());

		// Verify the file is no longer in SourceDir
		File sourceFile = new File(sourcePath + "/test.txt");
		Assert.assertFalse(sourceFile.exists());
	}
	
	/**
	 * After completion, stop the SFTP server and remove the temporary paths used.
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@After
	public void tearDown() throws IOException, InterruptedException {
		sshd.stop(true);

		FileUtils.deleteDirectory(new File(userPath));
	}
}
